/**
 * Created by krish004 on 10/4/16.
 */

/**
 * This is the main angular.js file that implements the entire application
 * we are using $rootScope.functionName() and $rootScope.variableName scope variables
 * to expose functions and
 * variables that can be used in the html page.
 *
 * The controller InviteController is the main controller where the various operations are performed.
 *
 * An important point to note is that all the images, lats, longitudes, images need to be loaded BEFORE
 * the page is actually loaded
 *
 * Below you will notice an app.config where the 'resolve' method(s) have the db calls to obtain all the
 * necessary info to load the page.
 * @type {any}
 */

// Represents the application along with angular module dependencies. Installing new dependencies involve
// during a bower install of the dependency, adding the script-tag in the index.html file and
// adding the module dependency name in the array here
app = angular.module("inviteKWebApp", ["ngRoute", "firebase", "angularMoment", "ngResource", "ngMap"]);

var firebase = undefined;

/**
 * Initialize the firebase instance before the page is loaded. This is called from index.html
 * @type {{set}}
 */
var initFirebaseStorage = (function () {
    var _args = {}; // private
    return {
        set: function (Args) {
            _args = Args;
            firebase = _args;
        }
    };
}());


/**
 * configure the app, resolve various dependencies through asynchronous calls.
 * The resolved variable e.g conversationDetails, activityInfoDetails etc
 * is passed on to the controller.
 *
 * Few points to note
 * 1. Each of the 'resolve'methods returns a promise. All methods in the resolve section have to
 * always return a promise
 *
 * 2.The :inviteId is the url parameter. We can add additional parameters if required in the $routeProvider.when
 * section below
 *
 * 3.test.html is a dummy html file to provide a dummy ng-view attribute. It can be ignored
 */
app.config(function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider.when('/:inviteId', {
        templateUrl: 'test.html',
        controller: 'InviteController',
        resolve: {
            conversationDetails: function ($q, $route) {
                var conversationInfo = $q.defer();
                // var inviteId = '8424B45A-76C8-4DFE-A1E1-552DC4BBD2F6';
                var inviteId = $route.current.params.inviteId;
                firebase.database().ref("conversations").child(inviteId).once('value').then(function (snapshot) {
                    var messages = snapshot.val();
                    if (messages != null) {
                        var last = null;
                        for (var x in messages) {
                            last = messages[x];
                        }
                        conversationInfo.resolve({lastMessage: last.messageBody, lastCommentAuthor: last.userName});
                    }
                });
                return conversationInfo.promise;
            },
            /**
             * Get all the info of the invite including the participants, palces, events etc
             * We make a specific asynchronous call to get the poster images for all the events, places
             * etc. The asynchronous .foreach call allows us to make multiple calls to resource in parallel
             * @param $q
             * @param $route
             * @returns {Promise}
             */
            activityInfoDetails:function($q, $route){

                var inviteId = $route.current.params.inviteId;
                var activityInfo = $q.defer();

                var storage = firebase.storage();
                var storageRef = storage.ref();

                firebase.database().ref('invitations').child(inviteId).once('value').then(function (snapshot) {

                    var invite = snapshot.val();

                    var things = [];

                    var activityInfoDetails = [];

                    var mainInfo = {};

                    mainInfo.eventName = invite.name;
                    mainInfo.date =  invite.date;
                    mainInfo.localTime = invite.localTime;
                    mainInfo.authorName = invite.author_name;
                    mainInfo.longitude = invite.where.longitude;
                    mainInfo.latitude = invite.where.latitude;
                    mainInfo.participants = invite.participants;

                    var todo = [];

                    if (invite.things.length > 1) {
                        var i = 1;
                        for (var x in invite.things) {
                            todo.push(invite.things[x]);
                            if (i != invite.things.length) {
                                todo.push("--");
                            }
                            i++;
                        }
                        things = todo;

                    } else {
                        things = invite.things;
                    }

                    var extractPosterImageUrls = function (element, doneCallback) {

                        if (element != '--') {
                            console.log(element);
                            var kind = element.kind;
                            var uniqueId = element.uniqueId;
                            var path = "images/" + kind + "/" + uniqueId + "/poster.jpg";
                            var posterRef = storageRef.child(path);

                            posterRef.getMetadata().then(function (metadata) {
                                var url = metadata.downloadURLs[0];
                                var imageBackGround = "background:url('" + url + "');";
                                var activityInfo = {};
                                activityInfo.imageUrl = imageBackGround;
                                activityInfo.name = element.name;
                                activityInfo.showThen = true;
                                activityInfoDetails.push(activityInfo);

                                // Nothing went wrong, so callback with a null error.
                                return doneCallback(null);
                            }).catch(function (error) {
                                // Uh-oh, an error occurred!
                                console.log('error in extractPosterImageUrls', error);
                                return doneCallback(error);
                            });
                        } else {
                            return doneCallback(null);
                        }
                    };

                    // asynchronously iterate through the list processing each member
                    async.each(things, extractPosterImageUrls, function (err) {
                        // extractPosterImageUrls has been called on each of the members
                        // of the 'thing' array
                        // so we're now done!
                        if (err) console.log(' error found in extracting poster image urls ', err);
                        else {

                            //NOTE the 'showThen' variable is used to determine whether we show
                            // the 'then' text between posters.We need to avoid showing the
                            // 'then' text below the last poster after iterating through the list
                            activityInfoDetails[activityInfoDetails.length - 1].showThen = false;
                            activityInfo.resolve({mainInfo: mainInfo, activityData: activityInfoDetails});
                            console.log("Finished!");
                        }
                    });

                });

                return activityInfo.promise;
            },

            /**
             * Get profile pics of the sender
             * @param $q
             * @param $route
             * @returns {Promise}
             */
            profilePic :function($q, $route){

                var profilePicInfo = $q.defer();
                var inviteId = $route.current.params.inviteId;
                var storage = firebase.storage();
                var storageRef = storage.ref();
                firebase.database().ref('invitations').child(inviteId).once('value').then(function (snapshot) {
                    var invite = snapshot.val();
                    var path = "images/ProfilePhoto/" + invite.author_id + "/poster.jpg";
                    var posterRef = storageRef.child(path);
                    posterRef.getMetadata().then(function (metadata) {
                        var url = metadata.downloadURLs[0];
                        profilePicInfo.resolve({imageUrl:url});
                    }).catch(function (error) {
                        // Uh-oh, an error occurred!
                    });
                });
                return profilePicInfo.promise;
            }
        }
    });
});

/**
 * The main controller. When this is invoked the conversationDetails, activityInfoDetails etc are already prefilled
 * with the necessary info because of the 'resolve' method in app.config section above
 */
app.controller("InviteController",
    function ($firebaseArray, $rootScope, moment, $scope, $routeParams, conversationDetails, activityInfoDetails, profilePic) {

        console.log('in invite controller', conversationDetails, activityInfoDetails, profilePic);

        $rootScope.activityDataList = activityInfoDetails.activityData;

        $rootScope.activityTitleName = activityInfoDetails.mainInfo.eventName;

        $rootScope.profilePic = profilePic.imageUrl;

        $rootScope.inviteSender = activityInfoDetails.mainInfo.authorName;

        var invite = activityInfoDetails.mainInfo;
        // date of the event
        var date = new Date(invite.date * 1000);
        var eventTime = moment(date);

        $rootScope.eventTime = eventTime.format("hh:mmA");
        $rootScope.eventDay = eventTime.format("MM.D.YY");

        // participants
        $rootScope.participants = invite.participants;

        // location
        $rootScope.latitude = invite.latitude;
        $rootScope.longitude = invite.longitude;
        // $rootScope.locationOnMap =
        $rootScope.placeName = invite.eventName;

        // last message in conversation
        $rootScope.lastMessage = conversationDetails.lastMessage;
        $rootScope.lastCommentAuthor = conversationDetails.lastCommentAuthor;

        $rootScope.getInitials =  function (item) {
            var ret = "?";

            if (item.firstName != null && item.firstName.length > 0) {
                ret = item.firstName[0];
            } else {
                ret = "";
            }
            if (item.lastName != null && item.lastName.length > 0) {
                ret = ret + "." + item.lastName[0];
            }
            return ret;
        };

        // am I going?
        var index = -1;

        // for (var x in invite.participants) {
        //     if (index >= 0) {
        //         break;
        //     }
        //
        //     var person = invite.participants[x];
        //     var phones = person.phones;
        //     for (var y in phones) {
        //         var pn = phones[y];
        //         if (pn == pThis.phone) {
        //             index = x;
        //             break;
        //         }
        //     }
        // }

        // $scope.mySelf = null;
        // if (index >= 0) {
        //     $scope.mySelf = invite.participants[index]
        // }

        //TODO this needs to be ported from the polymer world to
        // angular

        // Both the methods are already hooked to the html tags in the
        // index.html page. All the variables passed to the parent controller
        // are available to these methods below;
        $rootScope.acceptInvite = function(){
          console.log('accepted invite');
        };

        $rootScope.declineInvite = function($http){

            // we can use the built in $http method to make web services call
            // instead of $ajax call
            // Simple GET request example:
            // $http({
            //     method: 'GET',
            //     url: '/someUrl'
            // }).then(function successCallback(response) {
            //     // this callback will be called asynchronously
            //     // when the response is available
            // }, function errorCallback(response) {
            //     // called asynchronously if an error occurs
            //     // or server returns response with an error status.
            // });
            console.log('decline invite');


        };

    }
);

